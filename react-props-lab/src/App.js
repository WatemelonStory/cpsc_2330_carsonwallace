import "./App.css";
import Search from "./components/Search";
import List from "./components/List";
import React, { useState } from "react";

function App() {
    const [searchTerm, setSearchTerm] = useState("");

    return (
        <div className="App">
            {/* <input onChange={(event) => setInputText(event.target.value)} /> */}
            <h1>Search for a sport</h1>
            <div>(Or scroll and find it)</div>
            <Search setSearchTerm={setSearchTerm} />
            <List searchTerm={searchTerm} />

            {/* {!showPeople && <ViewPerson person={individual} setShowPeople={setShowPeople} />} */}
        </div>
    );
}

export default App;
