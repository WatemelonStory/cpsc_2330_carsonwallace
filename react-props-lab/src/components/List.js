import PropTypes from "prop-types";

/**
 *
 * @param {*} props
 *  * object - fields: name, age, title, setShowPerson (function)
 */
var Sports = [
    "pickleball",
    "soccer",
    "basketball",
    "tennis",
    "baseball",
    "golf",
    "running",
    "volleyball",
    "badminton",
    "swimming",
    "boxing",
    "table tennis",
    "skiing",
    "ice skating",
    "roller skating",
    "cricket",
    "rugby",
    "pool",
    "darts",
    "football",
    "bowling",
    "ice hockey",
    "surfing",
    "karate",
    "horse racing",
    "snowboarding",
    "skateboarding",
    "cycling",
    "archery",
    "fishing",
    "gymnastics",
    "figure skating",
    "rock climbing",
    "sumo wrestling",
    "taekwondo",
    "fencing",
    "water skiing",
    "jet skiing",
    "weight lifting",
    "scuba diving",
    "judo",
    "wind surfing",
    "kickboxing",
    "sky diving",
    "hang gliding",
    "bungee jumping"
];
function List(props) {
    return (
        <div>
            <select Size="5">
                {Sports.filter((sport) => sport.toLocaleLowerCase().includes(props.searchTerm.toLocaleLowerCase())).map(
                    (sport) => (
                        <option>{sport}</option>
                    )
                )}
            </select>
        </div>
    );
}

// ViewPerson.propTypes = {
//     // You can declare that a prop is a specific JS primitive. By default, these
//     // are all optional.
//     person: PropTypes.object.isRequired,
//     setShowPeople: PropTypes.func.isRequired,
//     setSomething: PropTypes.string.isRequired
// };

export default List;