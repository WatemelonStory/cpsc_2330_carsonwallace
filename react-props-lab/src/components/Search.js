/**
 * function component that greets people
 * accepts props
 *  expected: name
 */
function Search(props) {
    return <input onChange={(event) => props.setSearchTerm(event.target.value)} />;
}

export default Search;