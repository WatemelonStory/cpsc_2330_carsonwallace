import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";

import axios from "axios";

/**
 * displays pokemon information that user wants to view
 * @param {*} props 
 */
function PokemonView(props) {
    const [pokemon, setPokemon] = useState([]);
    async function listPokemon() {
        try {
            const response = await axios({
                method: "get",
                url: `https://web-app-pokemon.herokuapp.com/pokemon/${props.id}`,
                headers: {
                    "User-Id": "BuyMe"
                }
            });
            console.log(response.data);
            setPokemon(response.data);
        } catch (e) {
            console.log(e);
        }
    }
    {
        listPokemon();
    }
    return (
        <div>
            <h1>Name:{pokemon.name}</h1>
            <h2>Type:{pokemon.type}</h2>
            <h3>{pokemon.description}</h3>
            <img src={pokemon.image} alt="image of pokemon" />
            <div>
                <Button
                    variant="contained"
                    text-color="primary"
                    onClick={() => {
                        props.setCurrentComponent("list");
                    }}
                >
                    Back
                </Button>
            </div>
        </div>
    );
}

export default PokemonView;
