"use strict";

//TLDR: dont use VAR!
console.log(myVariable);
var myVariable = "carson";

//let
let myInt = 5;
let myString = "a atring";
let mtObject = {};
let myArray = (1, "string", [], {});

myInt = "five";

console.log(typeof myInt);

//== compares value
console.log("5" == 5);

//=== compares value and type

console.log("5" === 5);

//const
//const myValue = 5;
//myValue = 6;

//const myArrays = [];
//myArrays.push["some string"];

////const myObject = {};
//myObject.name = "Brian";

//functions

//standard
function myFunction(param1, param2) {
    if (typeof param1 !== "string") {
        alert("error");
    }
    return `${param1} ${1 === "1"} ${param2}`;
}
console.log(myFunction("Carson", "wallace"));

//es6 arrow/function expression
