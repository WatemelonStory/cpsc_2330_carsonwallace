import "./App.css";
import React, { useState } from "react";

/**
 * App component
 * @returns
 */
function App() {
    const [palindrome, setPalindrome] = useState("");
    const [param, setParam] = useState("");

    function isPalindrome() {
        const myStr = param.split("").reverse().join("");
        if (param === myStr) {
            setPalindrome("Yes");
        } else {
            setPalindrome("No");
        }
    }

    return (
        <div className="App">
            <h1>Welcome to my Palindrome app</h1>
            <div>
                {/* counter logic */}
                <div>Palindrome: {palindrome}</div>
                <button onClick={isPalindrome}>is Palindrome?</button>
            </div>
            <div>
                {/* onChange has one parameter - event (SyntheticEvent) */}
                <div>
                    <input onChange={(event) => setParam(event.target.value)}></input>
                </div>
            </div>
        </div>
    );
}

export default App;
