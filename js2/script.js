"use strict";
// // function myFunction(arg1, arg2) {
// //     //return is opional
// //     return arg1 + arg2;
// //     //~${arg1} ${arg2};
// // }
// // console.log(myFunction)(1, 2);

// // const expression = function (arg1, arg1) {
// //     return arg1 + arg2;
// // };

// // expression(1, 2);

// // const myArray = [1, 2, 3];
// // myArray.forEach(function (item, index) {
// //     console.log(`item; ${item} \n index: ${index}`);
// // });

// // const newExpression = (item, index) => {
// //     console.log(`item: ${item} \n index: ${index});
// // }
// //console.log(addstuff(1, 2));
// // function addstuff(arg1, arg2) {
// //     console.log(arg1 + arg2);
// // }

// //const addstuff = (arg1, arg2) => console.log(arg1 + arg2);
// /**
//  * Displays a alert on button click
//  */

// //documnt -refers to the entire html document
// console.log(document);
// //takes a css selector argument and returns the first element found
// const button2 = document.querySelector("#button2");
// //adding onclick attrabutes to message
// button2.onclick = () => logConsoleMessage();

// //takes a string ID of the element and returns one element
// const button3 = document.getElementById("button3");
// button3.addEventListener("click", logConsoleMessage);

// /**
//  * accepts function but cannot access arguments by default because the parentheses
//  * will cause it to execute on page load
//  */
//button3.addEventListener("click", () => ( displayAlert("this text will display");

// /**
//  *
//  */
// function logConsoleMessage() {
//     console.log("output to the console right here");
//     if (typeof alertText === "object") {
//         alertText = "fired by function";
//     }
// }

// function displayAlert(alertText = "This was caused by the button click event") {
//     alert(alertText);
// }
//takes argument -string - class name
//returns html element array

//const examples = document.getElementsByClassName("example");
//console.log(examples.length);
//console.log(examples[0]);

//returns the value of each index in an iterable colllection
// for (const example of examples) {
//     example.innerText = parseInt(Math.random() * 10);
// }
