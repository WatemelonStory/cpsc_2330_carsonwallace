"use strict";

//node uses an import called CommonJs
//CommonJs uses the keyword "require" to import files
const express = require("express");
const { v4: uuidv4 } = require("uuid");
const bodyParser = require("body-parser");
const PORT = 5000;

//main variable to set web service handlers to
const app = express();
//tells the service to attempt to read request bodies as JSON
app.use(bodyParser.json());

let videos = [
    { title: "Space Jam", releaseYear: 1995, available: true, id: uuidv4() },
    { title: "Star Wars", releaseYear: 1977, available: true, id: uuidv4() },
    { title: "Jurassic Park", releaseYear: 1993, available: true, id: uuidv4() }
];

//http://localhost:5000/health
/**
 * two paramters: url, function that gets called when handler is hit
 * function handler has two parameters:
 * * req: request (your api call that you sent to the service)
 * * res: outgoing response that gets sent back to you
 */
app.get("/health", (req, res) => {
    const resObject = {
        name: "Victor's Video Service",
        version: "1.0.0"
    };

    // res.send(JSON.stringify(resObject));
    res.status(200);
    res.json(resObject);
});

/**
 * GET all videos
 */
app.get("/videos", (req, res) => {
    res.status(200);
    res.json(videos);
});

/**
 * Get video by id
 * params: id - String/uuid
 */
app.get("/videos/:id", (req, res) => {
    const videoId = req.params.id;

    //first way
    // const filteredVideos = videos.filter((video) => video.id === videoId);
    // if (filteredVideos.length > 0) {
    //     res.json(filteredVideos[0]);
    // }

    //other way
    const foundVideo = videos.find((video) => video.id === videoId);
    if (foundVideo) {
        res.status(200);
        res.json(foundVideo);
    } else {
        res.status(404);
        res.json({ message: `Video with id of '${videoId}' not found` });
    }
});

app.post("/videos", (req, res) => {
    //we have access to req.body now because we pulled in the body-parser dependency
    // and set the line: app.use(bodyParser.json())
    const title = req.body.title;
    const releaseYear = req.body.releaseYear;

    if (title && releaseYear) {
        const newVideo = {
            id: uuidv4(),
            title: title,
            releaseYear: releaseYear,
            available: true
        };

        videos.push(newVideo);
        res.status(200);
        //returns a copy of the newly created video object
        res.json(newVideo);
    } else {
        //required fields are not included
        res.status(400);
        res.json({ message: "title & releaseYear are required fields in the body" });
    }
});

/**
 * Delete video by id
 * params: id - guid/uuidv4/string
 */
app.delete("/videos/:id", (req, res) => {
    const videoId = req.params.id;

    //returns an array index of the first item found by the expression/function passed in
    // returns -1 if not found
    const foundIndex = videos.findIndex((video) => video.id === videoId);

    //not using truthy here because if it's the first item in the array, it would fail
    if (foundIndex > -1) {
        videos.splice(foundIndex, 1);

        res.status(200);
        res.json({ message: "Item successfully deleted" });
    } else {
        res.status(404);
        res.json({
            message: `Video with id of '${videoId}' not found`
        });
    }
});
/**
 * modifies a vidoe -
 */
app.put("/videos/:id", (req, res) => {
    const videoId = req.params.id;
    //look for the paramaters in the request body
    const title = req.body.titles;
    const releaseYear = rew.body.releaseYear;

    if (title && releaseYear) {
        const foundIndex = videos.findIndex((video) => video.id === videoId);

        if (foundIndex > -1) {
            videos[foundIndex].title = title;
            videos[foundIndex].releaseYear = releaseYear;
            res.status(200);
            res.json(videos[foundIndex]);
        } else {
            res.status(404);
            res.json({ message: `Video with id of '${videoId}' not found` });
        }
    } else {
        res.status(400);
        res.json({ message: "title and release year are required paramaters in the body" });
    }
});

app.post("/videos/:id/checkout", (req, res) => {
    const videoId = req.params.id;
    const foundIndex = videos.findIndex((video) => video.id === videoId);

    if (foundIndex > -1) {
        if (videos[foundIndex].available) {
            videos[findIndex].available = false;
            res.status(200);
            res.json({ message: "video cheked out" });
        } else {
            res.status(409);
            res.json({ message: "video is already cheked out" });
        }
    } else {
        res.status(404);
        res.json({ message: `Video with id of '${videoId}' not found` });
    }
});

//tells application to start up and listen on specified port
// takes a second argument of a function to run when it's started (callback)
app.listen(PORT, () => console.log("Welcome to Victor's Video Venue"));

//start up the app through terminal w/ command: node app
