"use strict";

//variable to get Element
const coinImg = document.getElementById("coinImg");

/**
 * 
 * @param {Max} max 
 * Function that returns Randome number with the max
 * being the passed number
 */
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

//variable for innerHTML text
var uInner;

/**
 * changes file name alt or uInner if random number is 1 or 2
 */
function flipHT() {
    let fileName;
    let alt;
    var x = getRandomInt(2);
    if (x) {
        fileName = "heads.png";
        alt = "Heads Image";
        uInner = "Heads";
    } else {
        fileName = "tails.png";
        alt = "Tails Image";
        uInner = "Tails";
    }
    //changes the document
    document.getElementById("result").innerHTML = uInner;
    coinImg.src = fileName;
    coinImg.alt = alt;
}
