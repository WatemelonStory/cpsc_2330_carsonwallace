import React, { useState } from "react";

function MadLib() {
    const [name, setName] = useState("_____");
    const [thing, setthing] = useState("_____");
    const [adj, setadj] = useState("_____");

    return (
        <div>
            {/* onChange will update the name state variable with the input value */}
            <input onChange={(event) => setName(event.target.value)} placeholder="Person"></input>
            {/* whenever state changes, the dom will be re-rendered with the name variable */}

            {/* onChange will update the name state variable with the input value */}
            <input onChange={(event) => setthing(event.target.value)} placeholder="Thing"></input>
            {/* whenever state changes, the dom will be re-rendered with the name variable */}

            {/* onChange will update the name state variable with the input value */}
            <input onChange={(event) => setadj(event.target.value)} placeholder="Adjective"></input>
            {/* whenever state changes, the dom will be re-rendered with the name variable */}
            <div>
                Hello There, {name} how's your {thing} in your {adj} room
            </div>
        </div>
    );
}
export default MadLib;
