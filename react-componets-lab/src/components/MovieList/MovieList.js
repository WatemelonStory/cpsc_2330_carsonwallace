import React, { useState } from "react";
import "./MovieList.css";

const movies = [
    { title: "Billy Madison", releaseYear: 2000, description: "Adam Sandler as a kid" },
    { title: "Happy Gilmore", releaseYear: 1999, description: "Adam Sandler Golf movie" },
    { title: "Grown Ups", releaseYear: 2014, description: "Inappropriate and funny" }
];
function MovieList() {
    return (
        <div className="app">
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Release Year</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {movies.map((movie) => {
                        return (
                            <tr>
                                <td>{movie.title}</td>
                                <td>{movie.releaseYear}</td>
                                <td>{movie.description}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default MovieList;
