import "./App.css";
import React, { useState } from "react";
import MadLib from "./components/MadLib";
import MovieList from "./components/MovieList/MovieList";

function App() {
    const [showMadLib, setMadLib] = useState(false);
    const [showMovieList, setMovielist] = useState(false);

    return (
        <div className="App">
            <h1>Welcome to Movie and Madlibs</h1>
            {/* checked - initial value on component load
            onChange - update the state
            */}
            <input type="checkbox" checked={showMadLib} onChange={(event) => setMadLib(event.target.checked)}></input>
            {/* if showGreetings === true, then return yes, else return no */}
            <div>Show MadLib:{showMadLib ? "Showing" : "Hidden"}</div>
            {/* React expression for rendering. Checks first expression.  If false, stops there,
            if true, continues to the rest of the expression */}
            {showMadLib && <MadLib />}
            <input
                type="checkbox"
                checked={showMovieList}
                onChange={(event) => setMovielist(event.target.checked)}
            ></input>
            {/* if showGreetings === true, then return yes, else return no */}
            <div>Show Movie List: {showMovieList ? "Showing" : "Hidden"}</div>
            {/* React expression for rendering. Checks first expression.  If false, stops there,
            if true, continues to the rest of the expression */}
            {showMovieList && <MovieList />}
        </div>
    );
}

export default App;

/**
 * Ternary operator
 * Inline If State - normally for assignment or a quick expression
 *
 * const output = expression ? "outcome if true" : "outcome if false"
 *
 */
